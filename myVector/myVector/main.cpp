#include "vector.h"
//#include <vld.h>
#include <vector>

int main() {
	own::vector<std::string> val{ "one", "two", "three", "four", "five", "six", "seven", "eight" };
	for (auto element : val) {
		std::cout << element << " ";
	}
	std::cout << ":)\n";
}
