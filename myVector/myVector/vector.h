#pragma once
#include <iostream>
#include <cmath>
#include <initializer_list>
#include <exception>

//be careful when displaying dynamically allocated char address
//compiler gets char* type -> it's C style string, you need to cast to void*

namespace own {
	template<class T>
	class vector {
	private:
		int _capacity;
		int _size;
		int _pos;
		T* _buffer;

	public:
		class iterator;

		iterator begin();
		iterator end();

	public:
		vector();
		vector(int size);
		vector(std::initializer_list<T> elems);
		~vector();
		void changeCapacity();
		void print() const;
		void push_back(T value);
		T& get(int index) const;
	};

	template<class T>
	class vector<T>::iterator {
	private:
		int _pos;
		vector& _element;
	public:
		iterator(int pos, vector& element);
		iterator operator++(int);
		iterator operator++();
		T& operator*();
		bool operator!=(const iterator& other) const;
	};

	int getCapacity(int size);
}

//use typename to indicate a type
template<class T>
typename own::vector<T>::iterator own::vector<T>::begin() {
	return iterator(0, *this);
}

template<class T>
typename own::vector<T>::iterator own::vector<T>::end() {
	return iterator(_size, *this);
}


template<class T>
own::vector<T>::vector() : _capacity(1), _size(0), _pos(0), _buffer(nullptr) {
	_buffer = new T[_capacity]{};
}

template<class T>
own::vector<T>::vector(int size) : _size(size), _pos(0), _buffer(nullptr) {
	_capacity = own::getCapacity(size);
	_buffer = new T[_capacity]{};
}

//probably need to use cpp11 pointers
template<class T>
own::vector<T>::vector(std::initializer_list<T> elems) : _pos(0), _buffer(nullptr) {
	_size = elems.size();
	_capacity = own::getCapacity(_size);
	_buffer = new T[_capacity]{};
	T* bufferPointer = _buffer;
	for (auto elem : elems) {
		*_buffer = elem;
		_buffer++;
	}
	_buffer = bufferPointer;
}

template<class T>
own::vector<T>::~vector() {
	delete[] _buffer;
}

template<class T>
T& own::vector<T>::get(int index) const{
	return _buffer[index];
}

template<class T>
void own::vector<T>::push_back(T value) {
	_buffer[_pos++] = value;
	if (_pos == _size) { //change capacity of vector
		throw std::out_of_range("st");
	}
}

template<class T>
void own::vector<T>::print() const{
	for (int i = 0; i < _size; ++i) {
		std::cout << _buffer[i] << " ";
	}
}



template<class T>
own::vector<T>::iterator::iterator(int pos, vector& element) : _pos(pos), _element(element) {}

template<class T>
typename own::vector<T>::iterator own::vector<T>::iterator::operator++(int) {
	_pos++;
	return *this;
}

template<class T>
typename own::vector<T>::iterator own::vector<T>::iterator::operator++() {
	++_pos;
	return *this;
}

template<class T>
T& own::vector<T>::iterator::operator*() {
	return _element.get(_pos);
}

template<class T>
bool own::vector<T>::iterator::operator!=(const iterator& other) const {
	return _pos != other._pos;
}