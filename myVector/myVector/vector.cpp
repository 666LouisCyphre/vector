#include "vector.h"

int own::getCapacity(int size) {
	if (size < 4) size += 1;
	int logarithm = std::ceil(log2(size));
	return pow(2, logarithm); //can't you optimise it, and don't use logarithm?
}